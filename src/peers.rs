use std::collections::hash_set::HashSet;
use std::net::SocketAddrV4;
use std::sync::{Arc, Mutex};

pub type Peers = HashSet<SocketAddrV4>;
pub type APeers = Arc<Mutex<HashSet<SocketAddrV4>>>;

pub fn new_peers(addrs: SocketAddrV4) -> APeers {
    let mut h = HashSet::new();
    h.insert(addrs);
    Arc::new(Mutex::new(h))
}

pub fn add_peer(peers: &APeers, addrs: SocketAddrV4) {
    let mut peers = peers.lock().unwrap();
    peers.insert(addrs);
}

pub fn remove_peer(peers: &APeers, addrs: SocketAddrV4) {
    let mut peers = peers.lock().unwrap();
    peers.remove(&addrs);
}

pub fn overwrite_peers(peers: &APeers, new_peers: Peers) {
    let mut peers = peers.lock().unwrap();
    *peers = new_peers;
}

pub fn get_peers(peers: &APeers) -> Peers {
    let peers = peers.lock().unwrap();
    peers.clone()
}

pub fn is_in_peers(peers: &APeers, addrs: SocketAddrV4) -> bool {
    let peers = peers.lock().unwrap();
    peers.contains(&addrs)
}
