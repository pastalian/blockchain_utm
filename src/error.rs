use std::fmt;

pub type Result<T> = std::result::Result<T, Error>;

pub type Error = ErrorKind;

#[derive(Debug)]
pub enum ErrorKind {
    DataConflict,
    DataDuplicate,
    EmptyPool,
    InvalidHash,
    InvalidSignature,
    Outdated,
    Serialize,
    NonCritical,
}

impl From<bincode::Error> for Error {
    fn from(_err: bincode::Error) -> Self {
        ErrorKind::Serialize
    }
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self)
    }
}
