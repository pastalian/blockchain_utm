use std::net::SocketAddrV4;

use bincode::serialize;
use serde::{Deserialize, Serialize};

use crate::blockchain::{Block, Chain, SigTransaction};
use crate::peers::Peers;

#[derive(Serialize, Deserialize, Debug)]
pub enum Command {
    AddPeer,
    RemovePeer,
    Peers(Peers),
    RequestPeers,
    Ping,
    TX(SigTransaction),
    Block(Block),
    Chain(Chain),
    RequestChain,
    AcceptTx(u64),
    RejectTx(u64),
    ApproveTx(u64),
    DisapproveTx(u64),
    AcceptBlock,
    RejectBlock,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    pub cmd: Command,
    pub addrs: SocketAddrV4,
    pub channel: u8,
}

impl Message {
    pub fn new(cmd: Command, addrs: SocketAddrV4, channel: u8) -> Message {
        Message {
            cmd,
            addrs,
            channel,
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        serialize(self).unwrap()
    }
}
