use std::io::Write;
use std::net::SocketAddrV4;

use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;

use crate::peers::Peers;
use crate::protocol::Message;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub async fn send(msg: &Message, addrs: &SocketAddrV4) -> Result<()> {
    let msg = msg.to_bytes();

    let mut stream = TcpStream::connect(addrs).await?;

    stream.write_all(&msg).await.or_else(|e| Err(e.into()))
}

pub async fn send_to_peers(msg: &Message, peers: &Peers) {
    let msg = msg.to_bytes();

    for peer in peers.clone() {
        let m = msg.clone();

        tokio::spawn(async move {
            match TcpStream::connect(peer).await {
                Ok(mut s) => {
                    if let Err(e) = s.write_all(&m).await {
                        warn!("{}", e);
                    }
                }
                Err(e) => warn!("{}", e),
            }
        });
    }
}

pub fn sync_send_to_peers(msg: &Message, peers: &Peers) {
    let msg = msg.to_bytes();

    for peer in peers {
        match std::net::TcpStream::connect(peer) {
            Ok(mut s) => {
                if let Err(e) = s.write_all(&msg) {
                    warn!("{}", e);
                }
            }
            Err(e) => warn!("{}", e),
        };
    }
}
