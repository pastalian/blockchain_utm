//! Configuration builder.

use std::env;
use std::net::{Ipv4Addr, SocketAddrV4};
use std::process::Command;

use getopts::Options;
use regex::Regex;

pub struct CfgServer {
    pub addrs: SocketAddrV4,
    pub channels: Vec<u8>,
    pub peers: Vec<SocketAddrV4>,
}

impl CfgServer {
    pub fn new() -> Result<CfgServer, ()> {
        let args: Vec<String> = env::args().collect();

        let mut opts = Options::new();
        opts.optopt("p", "port", "listening port", "<PORT_NUM>");
        opts.optmulti("c", "channel", "joining channel (in order)", "<CH_ID>");
        opts.optmulti("a", "address", "joining address (in order)", "<ADDRESS>");
        opts.optflag("h", "help", "print this help menu");

        let matches = match opts.parse(&args[1..]) {
            Ok(m) => m,
            Err(_) => {
                print_usage(&args[0], opts);
                return Err(());
            }
        };

        if matches.opt_present("h") {
            print_usage(&args[0], opts);
            return Err(());
        }

        let port = match matches.opt_str("p") {
            Some(p) => match p.parse() {
                Ok(p) => p,
                Err(_) => {
                    print_usage(&args[0], opts);
                    return Err(());
                }
            },
            None => 50010,
        };

        let mut channels: Vec<u8> = matches
            .opt_strs("c")
            .iter()
            .map(|a| a.parse().unwrap())
            .collect();

        // Set default channel
        if channels.is_empty() {
            channels = vec![0];
        }

        let peers = matches
            .opt_strs("a")
            .iter()
            .map(|a| a.parse().unwrap())
            .collect();

        let ip = match get_local_ip() {
            Some(ip) => ip,
            None => {
                error!("Failed to get local ip address");
                return Err(());
            }
        };

        let addrs = SocketAddrV4::new(ip, port);

        Ok(CfgServer {
            addrs,
            peers,
            channels,
        })
    }
}

pub struct CfgClient {
    pub addrs: SocketAddrV4,
    pub node: SocketAddrV4,
    pub channel: u8,
    pub mode: char,
    pub dst: String,
}

impl CfgClient {
    pub fn new() -> Result<CfgClient, ()> {
        let args: Vec<String> = env::args().collect();

        let mut opts = Options::new();
        opts.optopt("p", "port", "listening port", "<PORT_NUM>");
        opts.optopt("c", "channel", "joining channel", "<CH_ID>");
        opts.optopt("a", "address", "connecting address", "<ADDRESS>");
        opts.optopt("d", "destination", "writing destination", "<PATH>");
        opts.optopt(
            "m",
            "mode",
            "run mode
             {c}: show chain
             {n}: send txs
             {v}: visualize",
            "{cnv}",
        );
        opts.optflag("h", "help", "print this help menu");

        let matches = match opts.parse(&args[1..]) {
            Ok(m) => m,
            Err(e) => {
                error!("{}", e);
                print_usage(&args[0], opts);
                return Err(());
            }
        };

        if matches.opt_present("h") {
            print_usage(&args[0], opts);
            return Err(());
        }

        let port = match matches.opt_str("p") {
            Some(p) => match p.parse() {
                Ok(p) => p,
                Err(e) => {
                    error!("{}", e);
                    print_usage(&args[0], opts);
                    return Err(());
                }
            },
            None => 60010,
        };

        let channel = match matches.opt_str("c") {
            Some(a) => match a.parse() {
                Ok(a) => a,
                Err(e) => {
                    error!("{}", e);
                    print_usage(&args[0], opts);
                    return Err(());
                }
            },
            // Set default channel
            None => 0,
        };

        let node = match matches.opt_str("a") {
            Some(a) => match a.parse() {
                Ok(a) => a,
                Err(e) => {
                    error!("{}", e);
                    print_usage(&args[0], opts);
                    return Err(());
                }
            },
            None => {
                error!("Input connecting address");
                print_usage(&args[0], opts);
                return Err(());
            }
        };

        let mode = match matches.opt_str("m") {
            Some(m) => match m.parse() {
                Ok(m) => m,
                Err(e) => {
                    error!("{}", e);
                    print_usage(&args[0], opts);
                    return Err(());
                }
            },
            None => 'n',
        };

        let dst = match matches.opt_str("d") {
            Some(p) => p,
            None => String::from("mission.plan"),
        };

        let ip = match get_local_ip() {
            Some(ip) => ip,
            None => {
                error!("Failed to get local IP");
                return Err(());
            }
        };

        let addrs = SocketAddrV4::new(ip, port);

        Ok(CfgClient {
            addrs,
            node,
            channel,
            mode,
            dst,
        })
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage:\n    {} [Options]", program);
    print!("{}", opts.usage(&brief));
}

pub fn get_local_ip() -> Option<Ipv4Addr> {
    let output = Command::new("ip")
        .args(&["-4", "address"])
        .output()
        .expect("failed to execute ip");

    let stdout = String::from_utf8(output.stdout).unwrap();

    let re = Regex::new(r".*inet (([0-9]*\.){3}[0-9]*)").unwrap();
    for cap in re.captures_iter(&stdout) {
        if &cap[1] != "127.0.0.1" {
            if let Ok(ip) = cap[1].parse() {
                return Some(ip);
            }
        }
    }

    None
}
