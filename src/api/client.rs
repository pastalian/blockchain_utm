use std::collections::HashSet;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::BufReader;
use std::net::SocketAddrV4;

use bincode::deserialize;
// use gnuplot::*;
use ring::signature::Ed25519KeyPair;
use tokio::io::AsyncReadExt;
use tokio::net::TcpListener;

use crate::blockchain::*;
use crate::mav::*;
use crate::protocol::{Command, Message};
use crate::receiver::receive_once;
use crate::sender::send;
use crate::util::timestamp_as_millis;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub async fn get_chain(addrs: SocketAddrV4, my_addrs: SocketAddrV4, ch: u8) -> Result<Chain> {
    send(&Message::new(Command::RequestChain, my_addrs, ch), &addrs).await?;

    let msg = receive_once(my_addrs).await?;

    match msg.cmd {
        Command::Chain(c) => Ok(c),
        _ => Err("Connection failed".into()),
    }
}

pub async fn send_tx(
    data: Data,
    addrs: SocketAddrV4,
    ch: u8,
    my_addrs: SocketAddrV4,
    key_pair: &Ed25519KeyPair,
) {
    send_stx(
        SigTransaction::new(data, my_addrs, key_pair),
        addrs,
        ch,
        my_addrs,
    )
    .await;
}

async fn send_stx(stx: SigTransaction, addrs: SocketAddrV4, ch: u8, my_addrs: SocketAddrV4) {
    if let Err(e) = send(&Message::new(Command::TX(stx), my_addrs, ch), &addrs).await {
        eprintln!("Err: Send tx to {} => {}", addrs, e)
    }
}

// pub fn visualize(chain: Chain) {
//     let mut a: Vec<&Data> = chain
//         .iter()
//         .flat_map(|b| {
//             b.txs
//                 .iter()
//                 .flat_map(|t| &t.tx.data)
//                 .collect::<Vec<&Data>>()
//         })
//         .collect();
//
//     a.sort_by_key(|d| d.time);
//
//     let mut b = Vec::new();
//     let mut prev = u64::max_value();
//     for d in a {
//         if d.time != prev {
//             let mut h = HashSet::new();
//             h.insert(d.morton);
//             b.push(h);
//         } else {
//             b.last_mut().unwrap().insert(d.morton);
//         }
//         prev = d.time;
//     }
//
//     show_morton(b);
// }

// fn show_morton(morton: Vec<HashSet<u32>>) {
//     let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
//     let mut itr = morton.iter().cycle();
//
//     let mut fg = Figure::new();
//
//     loop {
//         let hash = match itr.next() {
//             Some(h) => h,
//             None => {
//                 eprintln!("Err: No data to show");
//                 return;
//             }
//         };
//
//         let x: Vec<_> = hash.iter().map(|&k| morton_to_x(k, unit)).collect();
//         let y: Vec<_> = hash.iter().map(|&k| morton_to_y(k, unit)).collect();
//         let z: Vec<_> = hash.iter().map(|&k| morton_to_z(k, unit)).collect();
//
//         fg.clear_axes();
//         fg.axes3d()
//             .set_x_range(Fix(0.0), Fix(constant::MAX as f64))
//             .set_y_range(Fix(0.0), Fix(constant::MAX as f64))
//             .set_z_range(Fix(0.0), Fix(constant::MAX as f64))
//             .points(
//                 x.iter(),
//                 y.iter(),
//                 z.iter(),
//                 &[PointSymbol('O'), PointSize(2.0)],
//             );
//         fg.show().unwrap();
//         //        sleep(Duration::from_millis(10));
//     }
// }

pub async fn save_chain_as_mission(
    name: &str,
    addrs: SocketAddrV4,
    my_addrs: SocketAddrV4,
    ch: u8,
) {
    let chain = get_chain(addrs, my_addrs, ch).await.unwrap();

    for c in chain.as_vec() {
        for (i, stx) in c.txs.into_iter().enumerate() {
            let filename = format!("{}_{:03}", name, i);
            let mut file = File::create(filename).unwrap();
            let mut data = String::new();

            for (j, m) in stx.tx.data.mortons.into_iter().enumerate() {
                let x = morton_to_x(m) as f64 / 25.153129 * 0.0001 + 139.9379081;
                let y = morton_to_y(m) as f64 / 30.820188 * 0.0001 + 37.5219888;
                let z = morton_to_z(m) as f64;

                if j == 0 {
                    data.push_str(&mav_init(y, x));
                    data.push_str(&mav_takeoff(z));
                } else {
                    data.push_str(&mav_waypoint(z, y, x, j as u32));
                }
            }

            data.push_str(&mav_finish());
            file.write_all(data.as_bytes()).unwrap();
        }
    }
}

pub async fn store_and_send(
    filename: &str,
    data: Data,
    addrs: SocketAddrV4,
    ch: u8,
    my_addrs: SocketAddrV4,
    key_pair: &Ed25519KeyPair,
) {
    let mut file = OpenOptions::new().append(true).open(filename).unwrap();
    let stx = SigTransaction::new(data, my_addrs, key_pair);
    send_stx(stx, addrs, ch, my_addrs).await;
    let str = format!("{}\n", timestamp_as_millis());
    file.write_all(str.as_bytes()).unwrap();
}

pub async fn count_accepted(filename: &str, addrs: SocketAddrV4, ch: u8, my_addrs: SocketAddrV4) {
    let mut set = HashSet::new();
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    for line in reader.lines() {
        let id: u64 = line.unwrap().parse().unwrap();
        set.insert(id);
    }

    let total = set.len();
    let mut accepted = 0;

    let chain = get_chain(addrs, my_addrs, ch).await.unwrap();
    for b in chain.as_slice() {
        for stx in &b.txs {
            let id = stx.calc_id();
            if set.contains(&id) {
                accepted += 1;
            }
        }
    }

    println!("ACCEPTED: {} / {}", accepted, total);
}

pub async fn listen(addrs: SocketAddrV4, filename: &str) {
    let listener = TcpListener::bind(addrs).await.unwrap();
    let filename = filename.to_string();

    tokio::spawn(async move {
        loop {
            let mut file = OpenOptions::new()
                .append(true)
                .open(filename.clone())
                .unwrap();
            let mut stream = match listener.accept().await {
                Ok((s, _)) => s,
                Err(e) => {
                    error!("{}", e);
                    return;
                }
            };

            let mut buf = Vec::new();
            stream.read_to_end(&mut buf).await.unwrap();
            let msg: Message = deserialize(&buf).unwrap();

            tokio::spawn(async move {
                match msg.cmd {
                    Command::AcceptTx(id) | Command::RejectTx(id) => {
                        let str = format!("{}\n", timestamp_as_millis());
                        file.write_all(str.as_bytes()).unwrap();
                    }
                    _ => {}
                }
            });
        }
    });
}
