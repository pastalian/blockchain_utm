use std::collections::HashMap;
use std::net::SocketAddrV4;

use bincode::deserialize;
use tokio::io::AsyncReadExt;
use tokio::net::{TcpListener, TcpStream};
use tokio::time::{self, Duration};

use crate::blockchain::*;
use crate::peers::*;
use crate::protocol::{Command, Message};
use crate::routing;
use crate::sender::*;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

const SEND: &str = "\x1b[34m[SEND]\x1b[0m";
const RECV: &str = "\x1b[35m[RECV]\x1b[0m";

#[derive(Clone)]
struct Network {
    peers: APeers,
    chain: AChain,
    pool: MainPool,
    sub_pool: SubPool,
    holder: Holder,
}

pub struct Server {
    addrs: SocketAddrV4,
    listener: TcpListener,
    networks: HashMap<u8, Network>,
}

impl Server {
    pub async fn new(addrs: SocketAddrV4, channels: &[u8]) -> Server {
        println!("Server available at {}", addrs);
        println!("Running on channel: {:?}", channels);

        let mut networks = HashMap::new();

        for ch in channels {
            let peers = new_peers(addrs);
            let (chain, pool, sub_pool, holder) = new_block_chain(addrs);
            networks.insert(
                *ch,
                Network {
                    chain,
                    peers,
                    pool,
                    sub_pool,
                    holder,
                },
            );
        }

        let listener = TcpListener::bind(addrs).await.unwrap();

        Server {
            addrs,
            listener,
            networks,
        }
    }

    pub async fn join_peers(&self, peer: &SocketAddrV4, channel: u8) {
        let net = match self.networks.get(&channel) {
            Some(n) => n,
            None => {
                error!("Unkown channel: {}", channel);
                return;
            }
        };

        let msg = Message::new(Command::AddPeer, self.addrs, channel);

        if send(&msg, peer).await.is_ok() {
            add_peer(&net.peers, *peer);
        } else {
            error!("Failed to connect: {}", peer)
        }
    }

    pub fn spawn_generator(&self) {
        let addrs = self.addrs;

        for (ch, net) in &self.networks {
            let peers = net.peers.clone();
            let chain = net.chain.clone();
            let pool = net.pool.clone();
            let ch = *ch;

            tokio::spawn(async move {
                let mut interval = time::interval(Duration::from_secs(10));
                loop {
                    interval.tick().await;
                    generate_block(&chain, &pool, &peers, addrs, ch).await;
                }
            });
        }
    }

    pub async fn listen(&mut self) {
        loop {
            let socket = match self.listener.accept().await {
                Ok((s, _)) => s,
                Err(e) => {
                    error!("{}", e);
                    continue;
                }
            };

            let addrs = self.addrs;
            let networks = self.networks.clone();

            tokio::spawn(async move {
                if let Err(e) = handle_connection(socket, addrs, &networks).await {
                    error!("{}", e)
                }
            });
        }
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        for (ch, net) in &self.networks {
            let msg = Message::new(Command::RemovePeer, self.addrs, *ch);
            sync_send_to_peers(&msg, &get_peers(&net.peers));
        }
    }
}

async fn generate_block(
    chain: &AChain,
    pool: &MainPool,
    peers: &APeers,
    addrs: SocketAddrV4,
    ch: u8,
) {
    info!("Generating new block...");

    let (block, txs) = match generate_block_with_pool(chain, pool, addrs) {
        Ok((b, t)) => (b, t),
        Err(e) => {
            warn!("{}", e);
            return;
        }
    };

    let mut peers = get_peers(peers);
    peers.remove(&addrs);

    info!("{} Block", SEND);
    send_to_peers(&Message::new(Command::Block(block), addrs, ch), &peers).await;

    pool.remove_txs(&txs);
}

async fn handle_connection(
    mut stream: TcpStream,
    my_addrs: SocketAddrV4,
    networks: &HashMap<u8, Network>,
) -> Result<()> {
    let mut buf = Vec::new();

    stream.read_to_end(&mut buf).await?;
    // Close connection immediately
    drop(stream);

    let msg: Message = deserialize(&buf)?;
    let addrs = msg.addrs;
    let channel = msg.channel;

    let net = networks.get(&channel).ok_or("Unknown channel")?;

    match msg.cmd {
        Command::Ping => info!("{} Ping: {}", RECV, addrs),
        Command::AddPeer => handle_add_peer(addrs, channel, my_addrs, net).await,
        Command::RemovePeer => handle_remove_peer(addrs, channel, my_addrs, net).await,
        Command::TX(stx) => handle_tx(stx, addrs, channel, my_addrs, net).await?,
        Command::Block(block) => handle_block(block, addrs, channel, my_addrs, net).await?,
        Command::Chain(chain) => handle_chain(chain, net).await,
        Command::AcceptTx(id) => handle_accept_tx(id, addrs, channel, my_addrs, net).await?,
        Command::RejectTx(id) => handle_reject_tx(id, addrs, channel, my_addrs, net).await?,
        Command::ApproveTx(id) => approve_tx(id, &net.sub_pool, &net.pool),
        Command::DisapproveTx(id) => disapprove_tx(id, &net.sub_pool, &net.holder),
        Command::AcceptBlock => info!("{} AcceptBlock: {}", RECV, addrs),
        Command::RejectBlock => {
            warn!("{} RejectBlock: {}", RECV, addrs);

            let msg = Message::new(Command::Chain(get_chain(&net.chain)), my_addrs, channel);
            send(&msg, &addrs).await?;
        }
        Command::Peers(new_peers) => {
            info!("{} Peers: {}", RECV, addrs);

            overwrite_peers(&net.peers, new_peers);
        }
        Command::RequestPeers => {
            info!("{} RequestPeers: {}", RECV, addrs);

            let msg = Message::new(Command::Peers(get_peers(&net.peers)), my_addrs, channel);
            send(&msg, &addrs).await?;
        }
        Command::RequestChain => {
            info!("{} RequestChain: {}", RECV, addrs);

            let msg = Message::new(Command::Chain(get_chain(&net.chain)), my_addrs, channel);
            send(&msg, &addrs).await?;
        }
    }

    Ok(())
}

async fn handle_tx(
    stx: SigTransaction,
    addrs: SocketAddrV4,
    channel: u8,
    my_addrs: SocketAddrV4,
    net: &Network,
) -> Result<()> {
    let mut peers = get_peers(&net.peers);

    let id = stx.calc_id();

    // Decide whether to accept it locally, stock it in the local pool if ok
    match set_tx(stx.clone(), addrs, peers.len(), &net.sub_pool, &net.holder) {
        Ok(_) => {
            info!("{} TX: ACCEPT!", RECV);

            // Increment ACC counter and leave it alone until network approves
            accept_tx(id, &net.sub_pool, &net.pool);

            // Request from client, ask the network for a reply
            if !is_in_peers(&net.peers, addrs) {
                // Don't send to me again
                peers.remove(&my_addrs);

                let msg = Message::new(Command::TX(stx), my_addrs, channel);
                send_to_peers(&msg, &peers).await;

            // Reqest from the network, reply ok
            } else {
                let msg = Message::new(Command::AcceptTx(stx.calc_id()), my_addrs, channel);
                send(&msg, &addrs).await?;
            }
        }
        // Rejected, reply so
        Err(e) => {
            warn!("{} TX: {}", RECV, e);

            let msg = Message::new(Command::RejectTx(id), my_addrs, channel);
            send(&msg, &addrs).await?;

            let _new_route = routing::rerouting(&stx.tx.data, &net.holder.to_routing_vec(), 1000);
        }
    }
    Ok(())
}

async fn handle_block(
    block: Block,
    addrs: SocketAddrV4,
    channel: u8,
    my_addrs: SocketAddrV4,
    net: &Network,
) -> Result<()> {
    let line = format!("{} Block", RECV);

    let msg = match set_block(block, &net.chain, &net.pool) {
        Ok(_) => {
            info!("{}: ACCEPT!", line);

            Message::new(Command::AcceptBlock, my_addrs, channel)
        }
        Err(e) => {
            warn!("{}: {}", line, e);

            Message::new(Command::RejectBlock, my_addrs, channel)
        }
    };

    send(&msg, &addrs).await?;

    Ok(())
}

async fn handle_chain(chain: Chain, net: &Network) {
    let line = format!("{} Chain", RECV);

    match set_new_chain(chain, &net.chain, &net.pool) {
        Ok(_) => {
            info!("{}: accept!", line);
        }
        Err(e) => {
            warn!("{}: {}", line, e);
        }
    }
    // TODO: when chain fetch failed, definitely we have something to do
}

async fn handle_accept_tx(
    id: u64,
    addrs: SocketAddrV4,
    channel: u8,
    my_addrs: SocketAddrV4,
    net: &Network,
) -> Result<()> {
    info!("{} AcceptTx: {}", RECV, addrs);

    // Increment ACC counter, approve tx if count is enough
    if let Some(addrs) = accept_tx(id, &net.sub_pool, &net.pool) {
        // Send ACC response to the TX author
        let msg = Message::new(Command::AcceptTx(id), my_addrs, channel);
        send(&msg, &addrs).await?;

        let mut peers = get_peers(&net.peers);
        // Don't send to me
        peers.remove(&my_addrs);

        let msg = Message::new(Command::ApproveTx(id), my_addrs, channel);
        send_to_peers(&msg, &peers).await;
    }

    Ok(())
}

async fn handle_reject_tx(
    id: u64,
    addrs: SocketAddrV4,
    channel: u8,
    my_addrs: SocketAddrV4,
    net: &Network,
) -> Result<()> {
    info!("{} RejectTx: {}", RECV, addrs);

    // Increment REJ counter, reject tx if count is enough
    if let Some(addrs) = reject_tx(id, &net.sub_pool, &net.holder) {
        // Send REJ response to the TX author
        let msg = Message::new(Command::RejectTx(id), my_addrs, channel);
        send(&msg, &addrs).await?;

        let mut peers = get_peers(&net.peers);
        // Don't send to me
        peers.remove(&my_addrs);

        let msg = Message::new(Command::DisapproveTx(id), my_addrs, channel);
        send_to_peers(&msg, &peers).await;
    }

    Ok(())
}

async fn handle_add_peer(addrs: SocketAddrV4, channel: u8, my_addrs: SocketAddrV4, net: &Network) {
    info!("{} AddPeer: {}", RECV, addrs);

    add_peer(&net.peers, addrs);

    let mut peers = get_peers(&net.peers);
    let msg = Message::new(Command::Peers(peers.clone()), my_addrs, channel);

    // Send updated peer list to the network except me
    info!("{} Peers: {}", SEND, addrs);
    peers.remove(&my_addrs);
    send_to_peers(&msg, &peers).await;
}

async fn handle_remove_peer(
    addrs: SocketAddrV4,
    channel: u8,
    my_addrs: SocketAddrV4,
    net: &Network,
) {
    info!("{} RemovePeer: {}", RECV, addrs);

    remove_peer(&net.peers, addrs);

    let mut peers = get_peers(&net.peers);
    let msg = Message::new(Command::Peers(peers.clone()), my_addrs, channel);

    // Send updated peer list to the network except me
    info!("{} Peers: {}", SEND, addrs);
    peers.remove(&my_addrs);
    send_to_peers(&msg, &peers).await;
}
