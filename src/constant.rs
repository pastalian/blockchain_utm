//! Project level constant values.

pub const LEVEL: u32 = 8;
pub const MAX: f32 = 5_000.0;

pub const DX: f32 = 10.0;
pub const DZ: f32 = 5.0;
