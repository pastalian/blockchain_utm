use std::net::SocketAddrV4;

use bincode::deserialize;
use tokio::io::AsyncReadExt;
use tokio::net::TcpListener;

use crate::protocol::Message;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub async fn receive_once(addrs: SocketAddrV4) -> Result<Message> {
    let listener = TcpListener::bind(addrs).await?;

    let (mut stream, _) = listener.accept().await?;

    let mut buf = Vec::new();
    stream.read_to_end(&mut buf).await?;

    Ok(deserialize(&buf)?)
}
