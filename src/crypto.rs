//! Cryptography support.

use ring::rand;
use ring::signature::{Ed25519KeyPair, KeyPair, Signature, UnparsedPublicKey, ED25519};

use crate::error::*;

pub fn generate_keys() -> Ed25519KeyPair {
    let rng = rand::SystemRandom::new();
    let pkcs8_bytes = Ed25519KeyPair::generate_pkcs8(&rng).unwrap();

    Ed25519KeyPair::from_pkcs8(pkcs8_bytes.as_ref()).unwrap()
}

pub fn get_public_key(key_pair: &Ed25519KeyPair) -> &[u8] {
    key_pair.public_key().as_ref()
}

pub fn sign(msg: &[u8], key_pair: &Ed25519KeyPair) -> Signature {
    key_pair.sign(msg)
}

pub fn verify(msg: &[u8], signature: &[u8], public_key: &[u8]) -> Result<()> {
    let public_key = UnparsedPublicKey::new(&ED25519, public_key);
    public_key
        .verify(msg, signature)
        .map_err(|_| ErrorKind::InvalidSignature)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sign_and_verify() {
        let key_pair = generate_keys();
        let public = get_public_key(&key_pair);

        let msg = b"hello";
        let sig = sign(msg, &key_pair);

        let result = verify(msg, sig.as_ref(), public);

        assert!(result.is_ok())
    }
}
