pub fn mav_init(lat: f64, long: f64) -> String {
    format!(
        "{{
    \"fileType\": \"Plan\",
    \"geoFence\": {{
        \"circles\": [],
        \"polygons\": [],
        \"version\": 2
    }},
    \"groundStation\": \"QGroundControl\",
    \"rallyPoints\": {{
        \"points\": [],
        \"version\": 2
    }},
    \"version\": 1,
    \"mission\": {{
        \"cruiseSpeed\": 15,
        \"firmwareType\": 3,
        \"hoverSpeed\": 5,
        \"plannedHomePosition\": [
            {},
            {},
            0
        ],
        \"vehicleType\": 2,
        \"version\": 2,
        \"items\": [",
        lat, long
    )
}

pub fn mav_takeoff(alt: f64) -> String {
    format!(
        "
            {{
                \"autoContinue\": true,
                \"command\": 22,
                \"doJumpId\": 1,
                \"frame\": 3,
                \"params\": [15, 0, 0, null, 0, 0, {}],
                \"type\": \"SimpleItem\"
            }}",
        alt
    )
}

pub fn mav_waypoint(alt: f64, lat: f64, long: f64, idx: u32) -> String {
    format!(
        ",
            {{
                \"AMSLAltAboveTerrain\": null,
                \"Altitude\": {0},
                \"AltitudeMode\": 1,
                \"autoContinue\": true,
                \"command\": 16,
                \"doJumpId\": {3},
                \"frame\": 3,
                \"params\": [0, 0, 0, null, {1}, {2}, {0}],
                \"type\": \"SimpleItem\"
            }}",
        alt,
        lat,
        long,
        idx + 1
    )
}

pub fn mav_finish() -> String {
    format!(
        "
        ]
    }}
}}"
    )
}
