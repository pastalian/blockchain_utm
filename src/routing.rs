use rand::Rng;

use kdtree::distance::squared_euclidean;
use kdtree::KdTree;

use pathfinding::prelude::astar;

use crate::blockchain::{morton_to_idx, z_to_idx, Data};
use crate::constant;

pub type Boxel = [f64; 3];

fn create_alt() -> u32 {
    let mut rng = rand::thread_rng();

    let z = rng.gen_range(30.0, 100.0);
    z_to_idx(z)
}

pub fn create_waypoints(sx: u32, sy: u32, tx: u32, ty: u32, size: u32) -> Vec<Boxel> {
    let mut waypoints = Vec::new();

    let mut x = [sx, tx];
    let mut y = [sy, ty];
    x.sort();
    y.sort();

    let mut rng = rand::thread_rng();

    for _ in 0..size {
        let x_rng = rng.gen_range(x[0], x[1]) as f64;
        let y_rng = rng.gen_range(y[0], y[1]) as f64;
        let z_rng = create_alt() as f64;

        let p = [x_rng, y_rng, z_rng];

        waypoints.push(p);
    }

    waypoints
}

pub fn create_kdtree(boxels: &[Boxel]) -> KdTree<f64, usize, &Boxel> {
    let mut kdtree = KdTree::with_capacity(3, boxels.len());

    for (i, b) in boxels.iter().enumerate() {
        kdtree.add(b, i).unwrap();
    }

    kdtree
}

pub fn nearest_dist(p: &Boxel, k: &KdTree<f64, usize, &Boxel>) -> f64 {
    let d = k.nearest(p, 1, &squared_euclidean).unwrap();

    // TODO: error handling
    d[0].0
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Pos {
    pub x: u32,
    pub y: u32,
    pub z: u32,
}

impl Pos {
    pub fn new(x: u32, y: u32, z: u32) -> Self {
        Pos { x, y, z }
    }

    pub fn from_boxel(b: Boxel) -> Self {
        Pos {
            x: b[0] as u32,
            y: b[1] as u32,
            z: b[2] as u32,
        }
    }

    pub fn to_boxel(&self) -> Boxel {
        [self.x as f64, self.y as f64, self.z as f64]
    }
}

pub fn calc_astar(
    s: &Pos,
    t: &Pos,
    w: &[Boxel],
    obstacles: &KdTree<f64, usize, &Boxel>,
) -> Option<Vec<Pos>> {
    const WITHIN: f64 = constant::MAX as f64 * 10.0;
    let wp_tree = create_kdtree(w);

    let successor = |p: &Pos| -> Vec<(Pos, u32)> {
        let p_boxel = p.to_boxel();
        let p_cost = 1.0 / nearest_dist(&p_boxel, obstacles);

        // get nodes aroung p
        let neighbors = wp_tree
            .within(&p_boxel, WITHIN, &squared_euclidean)
            .unwrap();

        // inject cost
        neighbors
            .into_iter()
            .map(move |q| {
                let idx = *q.1;
                let q_cost = 1.0 / nearest_dist(&w[idx], obstacles);
                let d = (q_cost - p_cost).min(0.0) as u32;
                (Pos::from_boxel(w[idx]), d)
            })
            .collect()
    };

    let res = astar(
        s,
        |p| successor(p),
        |b| {
            (((b.x as i32 - t.x as i32).pow(2)
                + (b.y as i32 - t.y as i32).pow(2)
                + (b.y as i32 - t.y as i32).pow(2)) as f32)
                .sqrt() as u32
        },
        |b| b == t,
    );

    res.map(|r| r.0)
}

pub fn rerouting(d: &Data, o: &[[f64; 3]], size: u32) -> Option<Vec<Pos>> {
    let (sx, sy, _) = morton_to_idx(d.mortons[0]);
    let (tx, ty, _) = morton_to_idx(d.mortons[d.mortons.len() - 1]);

    // Lowest altitude
    let z = z_to_idx(30.0);

    let start = Pos::new(sx, sy, z);
    let goal = Pos::new(tx, ty, z);

    let mut waypoints = create_waypoints(sx, sy, tx, ty, size);
    waypoints.push(goal.to_boxel());

    let mut wtr = csv::Writer::from_path("waypoints.csv").unwrap();
    for o in &waypoints {
        wtr.write_record(&[o[0].to_string(), o[1].to_string(), o[2].to_string()])
            .unwrap();
    }

    let obstacles = create_kdtree(o);

    calc_astar(&start, &goal, &waypoints, &obstacles)
}

pub fn rerouting_data(d: &Data, o: &[[f64; 3]], size: u32) -> Option<Data> {
    let pos = rerouting(d, o, size)?;
    let pos: Vec<(u32, u32, u32)> = pos.into_iter().map(|p| (p.x, p.y, p.z)).collect();
    // test only
    let pos = [vec![(0, 0, 0)], pos].concat();

    Some(Data::from_idx(&pos, 0))
}
