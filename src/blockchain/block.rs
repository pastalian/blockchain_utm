use std::collections::BTreeSet;
use std::net::SocketAddrV4;

use bincode::serialize;
use ring::digest;
use serde::{Deserialize, Serialize};

use crate::blockchain::transaction::SigTransaction;
use crate::error::Result;
use crate::util::timestamp_as_millis;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Block {
    pub txs: BTreeSet<SigTransaction>,
    #[serde(with = "serde_bytes")]
    pub previous: Vec<u8>,
    pub addrs: SocketAddrV4,
    pub timestamp: u128,
}

impl Block {
    pub fn new(txs: BTreeSet<SigTransaction>, previous: Vec<u8>, addrs: SocketAddrV4) -> Block {
        Block {
            txs,
            previous,
            addrs,
            timestamp: timestamp_as_millis(),
        }
    }

    pub fn get_hash(&self) -> Vec<u8> {
        let encoded = serialize(self).unwrap();
        Vec::from(digest::digest(&digest::SHA256, &encoded).as_ref())
    }

    pub fn verify_signature(&self) -> Result<()> {
        for stx in &self.txs {
            stx.verify_signature()?
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::net::Ipv4Addr;

    use super::*;

    #[test]
    fn hash_should_be_different() {
        use std::{thread, time};
        let ten_mil = time::Duration::from_millis(10);

        let addrs = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 50000);

        let hash_a = Block::new(BTreeSet::new(), b"alpha".to_vec(), addrs).get_hash();
        thread::sleep(ten_mil);
        let hash_b = Block::new(BTreeSet::new(), b"alpha".to_vec(), addrs).get_hash();

        assert_ne!(hash_a, hash_b);
    }

    #[test]
    fn serialize_should_be_reversible() {
        use bincode::deserialize;
        let addrs = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 50000);
        let block = Block::new(BTreeSet::new(), b"alpha".to_vec(), addrs);
        let encoded = serialize(&block).unwrap();
        let decoded: Block = deserialize(&encoded).unwrap();

        assert_eq!(block, decoded);
    }
}
