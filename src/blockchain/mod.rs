use std::collections::BTreeSet;
use std::net::SocketAddrV4;
use std::sync::{Arc, Mutex};

pub use block::Block;
pub use chain::Chain;
pub use data::*;
pub use holder::Holder;
pub use pool::{MainPool, SubPool};
pub use transaction::SigTransaction;

use crate::error::{ErrorKind, Result};

mod block;
mod chain;
mod data;
mod holder;
mod pool;
mod transaction;

pub type AChain = Arc<Mutex<Chain>>;

pub fn new_block_chain(addrs: SocketAddrV4) -> (AChain, MainPool, SubPool, Holder) {
    (
        Arc::new(Mutex::new(Chain::new(addrs))),
        MainPool::new(),
        SubPool::new(),
        Holder::new(),
    )
}

pub fn generate_block_with_pool(
    chain: &AChain,
    pool: &MainPool,
    addrs: SocketAddrV4,
) -> Result<(Block, BTreeSet<SigTransaction>)> {
    let txs = pool.get_copy();

    if txs.is_empty() {
        return Err(ErrorKind::EmptyPool);
    }

    let mut chain = chain.lock().unwrap();

    let block = Block::new(txs.clone(), chain.last_hash(), addrs);
    chain.push(block.clone())?;

    Ok((block, txs))
}

pub fn set_tx(
    stx: SigTransaction,
    addrs: SocketAddrV4,
    max_res: usize,
    pool: &SubPool,
    holder: &Holder,
) -> Result<()> {
    stx.verify_signature()?;

    holder.insert(stx.tx.data.clone())?;

    if !pool.insert(stx, addrs, max_res) {
        return Err(ErrorKind::DataConflict);
    }

    Ok(())
}

pub fn accept_tx(id: u64, pool: &SubPool, main: &MainPool) -> Option<SocketAddrV4> {
    if pool.accept(id) {
        if let Some((stx, addrs)) = pool.remove(id) {
            main.insert(stx);
            return Some(addrs);
        }
    }

    None
}

pub fn reject_tx(id: u64, pool: &SubPool, holder: &Holder) -> Option<SocketAddrV4> {
    if pool.reject(id) {
        if let Some((stx, addrs)) = pool.remove(id) {
            holder.remove(&stx.tx.data);
            return Some(addrs);
        }
    }

    None
}

pub fn approve_tx(id: u64, pool: &SubPool, main: &MainPool) {
    if let Some((stx, _)) = pool.remove(id) {
        main.insert(stx);
    }
}

pub fn disapprove_tx(id: u64, pool: &SubPool, holder: &Holder) {
    if let Some((stx, _)) = pool.remove(id) {
        holder.remove(&stx.tx.data);
    }
}

pub fn set_block(block: Block, chain: &AChain, pool: &MainPool) -> Result<()> {
    block.verify_signature()?;

    chain.lock().unwrap().push(block.clone())?;

    // Remove pending txs which are already in the new block.
    pool.remove_txs(&block.txs);

    Ok(())
}

pub fn set_new_chain(new_chain: Chain, chain: &AChain, pool: &MainPool) -> Result<()> {
    if !new_chain.is_valid() {
        return Err(ErrorKind::InvalidHash);
    }

    let mut chain = chain.lock().unwrap();

    let mut orphan = chain.overwrite(new_chain)?;

    // Remove pending txs which is already in the new chain.
    for block in chain.as_slice() {
        pool.remove_txs(&block.txs);

        orphan = orphan.difference(&block.txs).cloned().collect();
    }

    // Add orphan txs to the pool.
    for o in orphan {
        let _ = pool.insert(o);
    }

    // Update holder

    Ok(())
}

pub fn get_chain(chain: &AChain) -> Chain {
    chain.lock().unwrap().clone()
}
