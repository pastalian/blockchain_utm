use std::collections::{BTreeMap, BTreeSet};
use std::net::SocketAddrV4;
use std::sync::{Arc, Mutex};

use crate::blockchain::transaction::SigTransaction;

type _MainPool = BTreeSet<SigTransaction>;

#[derive(Clone, Debug)]
pub struct MainPool(Arc<Mutex<_MainPool>>);

impl MainPool {
    pub fn new() -> Self {
        Self(Arc::new(Mutex::new(BTreeSet::new())))
    }

    pub fn insert(&self, stx: SigTransaction) -> bool {
        let mut pool = self.0.lock().unwrap();
        pool.insert(stx)
    }

    pub fn remove_txs(&self, txs: &BTreeSet<SigTransaction>) {
        let mut pool = self.0.lock().unwrap();
        *pool = pool.difference(txs).cloned().collect();
    }

    pub fn get_copy(&self) -> _MainPool {
        self.0.lock().unwrap().clone()
    }
}

#[derive(Clone, Debug)]
struct CountedTX {
    tx: SigTransaction,
    addrs: SocketAddrV4,
    max: usize,
    acc: usize,
    rej: usize,
}

impl CountedTX {
    fn new(tx: SigTransaction, addrs: SocketAddrV4, max: usize) -> Self {
        Self {
            tx,
            addrs,
            max,
            acc: 0,
            rej: 0,
        }
    }

    fn accept(&mut self) {
        self.acc += 1;
    }

    fn reject(&mut self) {
        self.rej += 1;
    }

    fn is_approved(&self) -> bool {
        self.acc * 3 >= self.max * 2
    }

    fn is_disapproved(&self) -> bool {
        self.rej * 3 > self.max
    }
}

type _SubPool = BTreeMap<u64, CountedTX>;

#[derive(Clone, Debug)]
pub struct SubPool(Arc<Mutex<_SubPool>>);

impl SubPool {
    pub fn new() -> Self {
        Self(Arc::new(Mutex::new(BTreeMap::new())))
    }

    pub fn insert(&self, stx: SigTransaction, addrs: SocketAddrV4, max_res: usize) -> bool {
        let mut pool = self.0.lock().unwrap();

        let id = stx.calc_id();
        if pool.contains_key(&id) {
            return false;
        }

        pool.insert(id, CountedTX::new(stx, addrs, max_res))
            .is_none()
    }

    pub fn remove(&self, id: u64) -> Option<(SigTransaction, SocketAddrV4)> {
        let mut pool = self.0.lock().unwrap();
        pool.remove(&id).map(|c| (c.tx, c.addrs))
    }

    pub fn accept(&self, id: u64) -> bool {
        let mut pool = self.0.lock().unwrap();

        if let Some(v) = pool.get_mut(&id) {
            v.accept();
            return v.is_approved();
        }

        false
    }

    pub fn reject(&self, id: u64) -> bool {
        let mut pool = self.0.lock().unwrap();

        if let Some(v) = pool.get_mut(&id) {
            v.reject();
            return v.is_disapproved();
        }

        false
    }
}
