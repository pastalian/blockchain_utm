use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::net::SocketAddrV4;

use serde::{Deserialize, Serialize};

use crate::blockchain::block::Block;
use crate::blockchain::SigTransaction;
use crate::error::{ErrorKind, Result};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Chain(Vec<Block>);

impl Chain {
    pub fn new(addrs: SocketAddrV4) -> Self {
        Self(vec![Block::new(BTreeSet::new(), b"alpha".to_vec(), addrs)])
    }

    pub fn push(&mut self, b: Block) -> Result<()> {
        if self.0.last().unwrap().get_hash() != b.previous {
            return Err(ErrorKind::InvalidHash);
        }

        self.0.push(b);

        Ok(())
    }

    /// new_chain: must be valid
    pub fn overwrite(&mut self, new_chain: Chain) -> Result<BTreeSet<SigTransaction>> {
        match self.0.len().cmp(&new_chain.0.len()) {
            Ordering::Greater => return Err(ErrorKind::Outdated),
            Ordering::Equal => {
                let old_last = self.0.last().unwrap();
                let new_last = new_chain.0.last().unwrap();
                match old_last.timestamp.cmp(&new_last.timestamp) {
                    Ordering::Less => return Err(ErrorKind::Outdated),
                    Ordering::Equal => {
                        if old_last.get_hash().lt(&new_last.get_hash()) {
                            return Err(ErrorKind::Outdated);
                        }
                    }
                    Ordering::Greater => {}
                }
            }
            Ordering::Less => {}
        }

        let orphan: BTreeSet<SigTransaction> = self
            .0
            .iter()
            .filter(|b| !new_chain.0.contains(*b))
            .flat_map(|b| &b.txs)
            .cloned()
            .collect();

        *self = new_chain;

        Ok(orphan)
    }

    pub fn is_valid(&self) -> bool {
        let mut it = self.0.iter();
        let mut prev = match it.next() {
            Some(p) => p,
            None => return false,
        };

        for cur in it {
            if prev.get_hash() != cur.previous {
                return false;
            }
            prev = cur;
        }

        true
    }

    pub fn to_vec(&self) -> Vec<Block> {
        self.0.to_vec()
    }

    pub fn as_slice(&self) -> &[Block] {
        &self.0.as_slice()
    }

    pub fn as_vec(self) -> Vec<Block> {
        self.0
    }

    pub fn last_hash(&self) -> Vec<u8> {
        self.0.last().unwrap().get_hash()
    }
}

#[cfg(test)]
mod tests {
    use std::net::Ipv4Addr;

    use super::*;

    #[test]
    fn check_orphan() {
        let addrs = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 50000);
        let addrs2 = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), 50001);
        let mut c1 = Chain::new(addrs);
        let mut c2 = Chain::new(addrs);
        c2.overwrite(c1.clone()).unwrap();

        let b1 = Block::new(BTreeSet::new(), c1.last_hash(), addrs);
        c1.push(b1.clone()).unwrap();
        c2.push(b1).unwrap();

        let b2 = Block::new(BTreeSet::new(), c1.last_hash(), addrs2);
        c1.push(b2.clone()).unwrap();

        let b3 = Block::new(BTreeSet::new(), c2.last_hash(), addrs);
        c2.push(b3).unwrap();

        let c1 = c1.to_vec();
        let c2 = c2.to_vec();

        let orphan: Vec<Block> = c1.iter().filter(|b| !c2.contains(*b)).cloned().collect();

        assert_eq!(orphan, vec![b2])
    }
}
