use serde::{Deserialize, Serialize};

use crate::constant;

/// Reservation data.
/// TODO: make time var Vec<u64> so we know important waypoints
#[derive(Serialize, Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Clone)]
pub struct Data {
    /// morton for each second
    pub mortons: Vec<u32>,
    /// Start time
    pub time: u64,
}

impl Data {
    pub fn new(mortons: Vec<u32>, time: u64) -> Data {
        Data { mortons, time }
    }

    /// Create `Data` from coord waypoints.
    /// Do not add z=0 waypoint data at the end. It will be added automatically.
    pub fn from_coord(c: &[(f32, f32, f32)], time: u64) -> Data {
        let mut x = c[0].0;
        let mut y = c[0].1;
        let mut z = c[0].2;

        let mut mortons = vec![get_morton_order(x, y, z)];

        for i in 1..c.len() {
            // Move z first
            let z_next = c[i].2;
            let (dz, adjust): (f32, fn(f32, f32) -> f32) = if z_next > z {
                (constant::DZ, f32::min)
            } else {
                (-constant::DZ, f32::max)
            };

            while z != z_next {
                z += dz;
                z = adjust(z, z_next);
                mortons.push(get_morton_order(x, y, z));
            }

            // Move x and y
            // TODO: doesn't adjust like z axis, too lazy
            // also different loop method, doesn't make sense but yeah
            let ax = c[i].0 - c[i - 1].0;
            let ay = c[i].1 - c[i - 1].1;
            let d = (ax.powi(2) + ay.powi(2)).sqrt();
            let dx = ax / d * constant::DX;
            let dy = ay / d * constant::DX;

            let t = (ax / dx).round() as i32;
            for _ in 0..t {
                x += dx;
                y += dy;
                mortons.push(get_morton_order(x, y, z));
            }
        }

        while z > 0.0 {
            z -= constant::DZ;
            z = z.max(0.0);
            mortons.push(get_morton_order(x, y, z));
        }

        Data { mortons, time }
    }

    pub fn from_idx(c: &[(u32, u32, u32)], time: u64) -> Data {
        let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
        let coords: Vec<(f32, f32, f32)> = c
            .iter()
            .map(|p| (p.0 as f32 * unit, p.1 as f32 * unit, p.2 as f32 * unit))
            .collect();
        Data::from_coord(&coords, time)
    }
}

pub fn morton_to_x(morton: u32) -> f32 {
    let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
    extract_bit(morton, 0) as f32 * unit
}

pub fn morton_to_y(morton: u32) -> f32 {
    let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
    extract_bit(morton, 1) as f32 * unit
}

pub fn morton_to_z(morton: u32) -> f32 {
    let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
    extract_bit(morton, 2) as f32 * unit
}

pub fn morton_to_idx(m: u32) -> (u32, u32, u32) {
    (extract_bit(m, 0), extract_bit(m, 1), extract_bit(m, 2))
}

pub fn z_to_idx(z: f32) -> u32 {
    morton_to_idx(get_morton_order(0.0, 0.0, z)).2
}

fn extract_bit(morton: u32, idx: u32) -> u32 {
    let mut mask = 1;
    let mut ret = 0;
    for i in 0..constant::LEVEL {
        ret |= (morton >> (3 * i - i + idx)) & mask;
        mask <<= 1;
    }
    ret
}

fn get_morton_order(x: f32, y: f32, z: f32) -> u32 {
    let unit = constant::MAX / 2u32.pow(constant::LEVEL) as f32;
    let (x, y, z) = (x.max(0.0), y.max(0.0), z.max(0.0));
    let (x, y, z) = (
        x.min(constant::MAX - 1.0),
        y.min(constant::MAX - 1.0),
        z.min(constant::MAX - 1.0),
    );
    let (x, y, z) = ((x / unit) as u32, (y / unit) as u32, (z / unit) as u32);

    separate_two_bit(x) | separate_two_bit(y) << 1 | separate_two_bit(z) << 2
}

fn separate_two_bit(n: u32) -> u32 {
    let mut n = n;
    n = (n | n << 8) & 0x0000_f00f;
    n = (n | n << 4) & 0x000c_30c3;
    (n | n << 2) & 0x0024_9249
}

///// Get a invert level of the morton order range.
///// Actual level is obtained by subtracting from the max level.
//fn get_level(min: u32, max: u32) -> u32 {
//    let mut n = min ^ max;
//    let mut level = 0;
//    let mut cnt = 0;
//
//    while n != 0 {
//        if n & 0b11 != 0 {
//            level = cnt;
//        }
//        cnt += 1;
//        n >>= 2;
//    }
//    level
//}
//
///// Get a morton order in given invert level.
//fn convert_morton_order_with_level(n: u32, inv_level: u32) -> u32 {
//    let s = 2i32.pow(inv_level + 1);
//    n >> s
//}
//
//#[cfg(test)]
//mod tests {
//    use super::*;
//
//    #[test]
//    fn test_morton() {
//        let (x, y, z) = (0b101, 0b011, 0b100);
//        let n = get_morton_order(x, y, z);
//        assert_eq!(n, 0b101_010_011);
//
//        let a = get_level(44, 47);
//        let b = convert_morton_order_with_level(47, a);
//        assert_eq!(b, 11);
//        let a = get_level(15, 60);
//        let b = convert_morton_order_with_level(60, a);
//        assert_eq!(b, 0);
//    }
//}
