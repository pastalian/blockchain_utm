use std::cmp::Ordering;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::net::SocketAddrV4;

use bincode::serialize;
use ring::signature::Ed25519KeyPair;
use serde::{Deserialize, Serialize};

use crate::blockchain::data::Data;
use crate::crypto::{get_public_key, sign, verify};
use crate::error::Result;
use crate::util::timestamp_as_millis;

#[derive(Serialize, Deserialize, Eq, PartialEq, Hash, Debug, Clone)]
pub struct Transaction {
    pub data: Data,
    addrs: SocketAddrV4,
    #[serde(with = "serde_bytes")]
    pub_key: Vec<u8>,
    timestamp: u128,
}

impl PartialOrd for Transaction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Transaction {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.timestamp.cmp(&other.timestamp) {
            Ordering::Equal => self.data.cmp(&other.data),
            c => c,
        }
    }
}

impl Transaction {
    fn new(data: Data, addrs: SocketAddrV4, pub_key: Vec<u8>) -> Transaction {
        Transaction {
            data,
            addrs,
            pub_key,
            timestamp: timestamp_as_millis(),
        }
    }
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash, Debug, Clone)]
pub struct SigTransaction {
    pub tx: Transaction,
    #[serde(with = "serde_bytes")]
    signature: Vec<u8>,
}

impl SigTransaction {
    pub fn new(data: Data, addrs: SocketAddrV4, key_pair: &Ed25519KeyPair) -> SigTransaction {
        let msg = serialize(&data).unwrap();
        let signature = sign(&msg, key_pair).as_ref().to_vec();
        let pub_key = get_public_key(key_pair).to_vec();
        SigTransaction {
            tx: Transaction::new(data, addrs, pub_key),
            signature,
        }
    }

    pub fn verify_signature(&self) -> Result<()> {
        let msg = serialize(&self.tx.data)?;

        verify(&msg, &self.signature, &self.tx.pub_key)
    }

    pub fn calc_id(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }
}
