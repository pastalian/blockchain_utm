use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};

use crate::blockchain::data::{morton_to_idx, Data};
use crate::error::{ErrorKind, Result};

type _Holder = HashMap<u64, HashSet<u32>>;

#[derive(Clone, Debug)]
pub struct Holder(Arc<Mutex<_Holder>>);

impl Holder {
    pub fn new() -> Self {
        Self(Arc::new(Mutex::new(HashMap::new())))
    }

    pub fn insert(&self, data: Data) -> Result<()> {
        let mut holder = self.0.lock().unwrap();

        let mut t = data.time;

        for m in data.mortons {
            match holder.get_mut(&t) {
                Some(h) => {
                    if !h.insert(m) {
                        return Err(ErrorKind::DataConflict);
                    }
                }
                None => {
                    let mut h = HashSet::new();
                    h.insert(m);
                    holder.insert(t, h);
                }
            }

            t += 1;
        }

        Ok(())
    }

    pub fn remove(&self, data: &Data) {
        let mut holder = self.0.lock().unwrap();

        let mut t = data.time;

        for m in &data.mortons {
            if let Some(h) = holder.get_mut(&t) {
                h.remove(m);
            }

            t += 1;
        }
    }

    // Convert for rerouting without time information
    pub fn to_routing_vec(&self) -> Vec<[f64; 3]> {
        let c = self.0.lock().unwrap().clone();
        c.into_iter()
            .flat_map(|(_, h)| {
                h.into_iter()
                    .map(|m| {
                        let (x, y, z) = morton_to_idx(m);
                        [x as f64, y as f64, z as f64]
                    })
                    .collect::<Vec<[f64; 3]>>()
            })
            .collect::<Vec<[f64; 3]>>()
    }
}
