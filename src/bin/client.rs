use std::net::SocketAddrV4;
use std::thread::sleep;
use std::time::Duration;

use log::*;
use rand::prelude::*;
use tokio::fs::File;

use bb_utm::api::client::*;
use bb_utm::config::CfgClient;
use bb_utm::constant;
use bb_utm::crypto::generate_keys;
use bb_utm::Data;

const SIM_SIZE: i32 = 1000;
const HASH_LIST: &str = "hash_list.txt";
const RES_LIST: &str = "result_list.txt";

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let cfg = match CfgClient::new() {
        Ok(c) => c,
        Err(_) => return,
    };

    let node = cfg.node;
    let addrs = cfg.addrs;
    let ch = cfg.channel;

    match cfg.mode {
        'n' => send_random_data(node, addrs, ch).await,
        'g' => match get_chain(node, addrs, ch).await {
            Ok(c) => println!("{:?}", c),
            Err(e) => panic!("{}", e),
        },
        'v' => {
            println!("This feature is disabled.");
            return;
            // match get_chain(node, addrs, ch).await {
            //     Ok(c) => visualize(c),
            //     Err(e) => panic!("{}", e),
            // }
        }
        's' => save_chain_as_mission(&cfg.dst, node, addrs, ch).await,
        't' => {
            for i in 0..100 {
                println!("Sending tx pack id: {}", i);
                send_random_data(node, addrs, ch).await;
            }
        }
        'a' => {
            File::create(HASH_LIST).await.unwrap();
            File::create(RES_LIST).await.unwrap();

            // generate `SIM_SIZE` data
            let dataset: Vec<Data> = (0..SIM_SIZE).map(|_| gen_data()).collect();

            listen(addrs, RES_LIST).await;

            // store all hash of the generated transactions to the text file
            let mut cnt = 0;
            for d in dataset {
                cnt += 1;
                if cnt % 100 == 0 {
                    println!("NUM: {}", cnt);
                }
                let key_pair = generate_keys();
                store_and_send(HASH_LIST, d, node, ch, addrs, &key_pair).await;
            }

            sleep(Duration::from_millis(1000));
        }
        'b' => count_accepted(HASH_LIST, node, ch, addrs).await,
        'c' => {
            File::create(HASH_LIST).await.unwrap();

            // generate `SIM_SIZE` data
            let dataset: Vec<Data> = (0..SIM_SIZE).map(|_| gen_data()).collect();

            let p = 50010;
            let mut nodes: Vec<SocketAddrV4> = Vec::new();
            for i in 5..22 {
                if i == 7 {
                    continue;
                }
                nodes.push(format!("192.168.2.1{:02}:{}", i, p).parse().unwrap());
            }

            // store all hash of the generated transactions to the text file
            let mut rng = rand::thread_rng();
            let mut cnt = 0;
            for d in dataset {
                cnt += 1;
                if cnt % 100 == 0 {
                    println!("NUM: {}", cnt);
                }
                let key_pair = generate_keys();
                let idx = rng.gen::<usize>() % 16;
                store_and_send(HASH_LIST, d, nodes[idx], ch, addrs, &key_pair).await;
            }
        }
        _ => {}
    }
}

async fn send_data(data: Data, node: SocketAddrV4, ch: u8, addrs: SocketAddrV4) {
    let key_pair = generate_keys();
    send_tx(data, node, ch, addrs, &key_pair).await;
}

async fn send_random_data(node: SocketAddrV4, addrs: SocketAddrV4, ch: u8) {
    let data = gen_data();
    debug!("{:?}", data);
    send_data(data, node, ch, addrs).await;
}

fn gen_data() -> Data {
    let mut rng = rand::thread_rng();

    let sx = rng.gen::<f32>() * constant::MAX;
    let sy = rng.gen::<f32>() * constant::MAX;

    let alt = rng.gen::<f32>() * constant::MAX;

    let tx = rng.gen::<f32>() * constant::MAX;
    let ty = rng.gen::<f32>() * constant::MAX;

    let st = 0;

    let coords = vec![(sx, sy, 0.0), (tx, ty, alt)];
    Data::from_coord(&coords, st)
}
