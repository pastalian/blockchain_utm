use std::env;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::{thread, time};

use bb_utm::api::server::Server;
use bb_utm::config::get_local_ip;

//use tokio::runtime::Builder;

#[tokio::main]
async fn main() {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || r.store(false, Ordering::SeqCst)).unwrap();

    let num_server = match env::args().nth(1) {
        Some(n) => n.parse().unwrap(),
        None => 2,
    };

    let mut servers = Vec::new();
    let ip = get_local_ip().unwrap();
    let mut port = 50010;
    let channels = vec![0];

    //    let mut rt = Builder::new()
    //        .threaded_scheduler()
    //        .enable_all()
    //        .max_threads(4)
    //        .build()
    //        .unwrap();

    //    rt.block_on(async {
    for _ in 0..num_server {
        let addrs = format!("{}:{}", ip, port).parse().unwrap();
        servers.push(Server::new(addrs, &channels).await);
        port += 1;
    }

    let mut cnt = -1i32;
    for mut s in servers {
        s.spawn_generator();
        let peer = format!("{}:{}", ip, 50010 + cnt).parse().unwrap();
        s.join_peers(&peer, 0).await;
        cnt += 1;

        tokio::spawn(async move {
            s.listen().await;
        });
    }
    //    });

    // keep this program alive
    while running.load(Ordering::SeqCst) {
        thread::sleep(time::Duration::from_millis(100));
    }
}
