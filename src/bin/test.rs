use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Instant;

use rand::Rng;

use bb_utm::blockchain::{morton_to_idx, Data, Holder};
use bb_utm::constant;
use bb_utm::routing;

fn create_obstacles() -> Holder {
    const SIZE: u32 = 50;

    let mut rng = rand::thread_rng();
    let holder = Holder::new();

    for _ in 0..SIZE {
        // left to right
        let mut lr = || {
            let sy = rng.gen_range(50.0, constant::MAX - 50.0);
            let ty = constant::MAX - sy - 1.0;
            let z = rng.gen_range(30.0, 100.0);
            let s = (50.0, sy, 0.0);
            let t = (constant::MAX - 50.0, ty, z);
            let data = Data::from_coord(&vec![s, t], 0);
            holder.insert(data).is_err()
        };
        while lr() == true {}

        // botoom to top
        let mut bt = || {
            let sx = rng.gen_range(50.0, constant::MAX - 50.0);
            let tx = constant::MAX - sx - 1.0;
            let z = rng.gen_range(30.0, 100.0);
            let s = (sx, 50.0, 0.0);
            let t = (tx, constant::MAX - 50.0, z);
            let data = Data::from_coord(&vec![s, t], 0);
            holder.insert(data).is_err()
        };
        while bt() == true {}
    }

    holder
}

fn _rerouting_test() {
    let holder = create_obstacles();
    let o = holder.to_routing_vec();

    let mut wtr = csv::Writer::from_path("obstacles.csv").unwrap();
    for o in &o {
        wtr.write_record(&[o[0].to_string(), o[1].to_string(), o[2].to_string()])
            .unwrap();
    }

    let start = (0.0, 0.0, 0.0);
    let goal = (constant::MAX - 1.0, constant::MAX - 1.0, 30.0);
    let data = Data::from_coord(&vec![start, goal], 0);

    match holder.insert(data.clone()) {
        Ok(_) => {
            println!("NO COLLISION FROM THE BEGGINING");
            holder.remove(&data);
        }
        Err(_) => println!("collision detected")
    }

    let mut wtr = csv::Writer::from_path("first.csv").unwrap();
    for d in &data.mortons {
        let (x, y, z) = morton_to_idx(*d);
        wtr.write_record(&[x.to_string(), y.to_string(), z.to_string()])
            .unwrap();
    }

    let res = routing::rerouting_data(&data, &o, 1000).unwrap();
    let mut wtr = csv::Writer::from_path("result.csv").unwrap();
    for d in res.mortons {
        let (x, y, z) = morton_to_idx(d);
        wtr.write_record(&[x.to_string(), y.to_string(), z.to_string()])
            .unwrap();
    }
}

fn _rerouting_exp() -> Vec<i32> {
    // Generate reserved data
    let holder = create_obstacles();
    let o = holder.to_routing_vec();

    // Data to be reserved
    let start = (0.0, 0.0, 0.0);
    let goal = (constant::MAX - 1.0, constant::MAX - 1.0, 30.0);
    let data = Data::from_coord(&vec![start, goal], 0);

    // Rerouting with sample size n
    let size = vec![10, 100, 1000];
    let mut result = Vec::new();
    for n in size {
        match routing::rerouting_data(&data, &o, n) {
            Some(res) => {
                // Collision detection
                match holder.insert(res.clone()) {
                    Ok(_) => {
                        result.push(0);

                        // Reset holder for the next detection
                        holder.remove(&res);
                    }
                    Err(_) => result.push(1)
                }
            }
            None => {
                // Generation failure is treated as a rejection
                result.push(1);
            }
        }
    }
    result
}

fn _helper() {
    let rej = Arc::new(Mutex::new(vec![0, 0, 0]));
    let outer = 10;
    let inner = 10;

    for i in 0..outer {
        let mut handles = vec![];
        for _ in 0..inner {
            let rej = rej.clone();
            let handle = thread::spawn(move || {
                let res = _rerouting_exp();
                let mut rej = rej.lock().unwrap();
                for i in 0..3 {
                    rej[i] = rej[i] + res[i];
                }
            });
            handles.push(handle);
        }

        for handle in handles {
            handle.join().unwrap();
        }
        println!("#{}: {:?}", inner * (i + 1), rej.lock().unwrap());
    }

    println!("{:?}", rej.lock().unwrap());
}

fn _measure() {
    let o = create_obstacles().to_routing_vec();

    let start = (0.0, 0.0, 0.0);
    let goal = (constant::MAX - 1.0, constant::MAX - 1.0, 30.0);
    let data = Data::from_coord(&vec![start, goal], 0);

    let size = vec![100, 1000, 10000];
    for n in size {
        let mut sum = 0.0;
        for _ in 0..10 {
            let t = Instant::now();

            routing::rerouting_data(&data, &o, n);

            let duration = t.elapsed();
            sum = sum + duration.as_secs_f32();
            println!("{:?}", duration);
        }
        println!("size {}: {}", n, sum / 10.0);
    }
}

fn main() {
    _helper();
    //_measure();
    //_rerouting_test();
}
