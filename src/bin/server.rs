use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::{thread, time};

use bb_utm::api::server::Server;
use bb_utm::config::CfgServer;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || r.store(false, Ordering::SeqCst)).unwrap();

    let cfg = match CfgServer::new() {
        Ok(c) => c,
        Err(_) => return,
    };

    let mut server = Server::new(cfg.addrs, &cfg.channels).await;

    server.spawn_generator();

    for (i, p) in cfg.peers.iter().enumerate() {
        server.join_peers(p, cfg.channels[i]).await;
    }

    // main server loop
    tokio::spawn(async move {
        server.listen().await;
    });

    // keep this program alive
    while running.load(Ordering::SeqCst) {
        thread::sleep(time::Duration::from_millis(100));
    }
}
