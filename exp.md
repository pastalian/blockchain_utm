## 16台での航路予約テスト
### インストール
#### Prebuilt binary
[v0.2.1](https://gitlab.com/pastalian/blockchain_utm/-/tags/v0.2.1)

#### Manual install
[README.md](./README.md)

### ネットワーク作成
RPi1を初期ノードとして、RPi2~RPi16を別のノードに接続させる。

#### RPi1
チャンネル0を作成
* ip: local ip（例: 192.168.1.2）
* port: 50010 (`-p`で変更可能）
```
./server -c 0
```

#### RPi2~RPi16
チャンネル0のノードに参加
* 接続先を`-a`で指定
    * 例: `192.168.1.2:50010`
    * 注: すでにチャンネルに参加しているノードを指定（RPi1など）
```
./server -c 0 -a 192.168.1.2:50010
```

### クライアント
実際に予約をリクエストする
* 接続先を`-a`で指定
    * 例: `192.168.1.2:50010`

```
./client -c 0 -a 192.168.1.2:50010 -m a
```

`hash_list.txt`と`result_list.txt`が生成される。
中にtimestampが入っているので差を計算すれば応答時間がわかる（はず）。

## 航路生成テスト
```
./test
```
waypoint数100,1000,10000での計算が行われ、
`#idx: [100でのreject, 1000でのreject, 10000でのreject]`の出力がでる。
