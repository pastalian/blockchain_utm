# Blockchain UTM
This repository is my lab project which aims to create blockchain based UTM application.
Written in Rust.

## Features
Under heavy development.

## Download
Pre-build packages for Linux (x86_64, armv7) are found on the [Releases](https://gitlab.com/pastalian/blockchain_utm/-/releases) page.

## Build
To build from source, you need to install [Rust](https://www.rust-lang.org/)  
-> [rustup](https://rustup.rs/) is recommended.

After installing Rust,

```sh
git clone https://gitlab.com/pastalian/blockchain_utm.git
cd blockchain_utm
```

```sh
cargo build --release
```

Binary files should be in `target/release/`.

## Usage

### Server
```
Usage:
    server [Options]

Options:
    -p, --port <PORT_NUM>
                        listening port
    -c, --channel <CH_ID>
                        joining channel (in order)
    -a, --address <ADDRESS>
                        joining address (in order)
    -h, --help          print this help menu
```

### Client
Currently, only for test purpose.
```
Usage:
    client [Options]

Options:
    -p, --port <PORT_NUM>
                        listening port
    -c, --channel <CH_ID>
                        joining channel
    -a, --address <ADDRESS>
                        joining address
    -m, --mode {cnv}    run mode
                        {c}: show chain
                        {n}: send txs
                        {v}: visualize
    -h, --help          print this help menu
```
